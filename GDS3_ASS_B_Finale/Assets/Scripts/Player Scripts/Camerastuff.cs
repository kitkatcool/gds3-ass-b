﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camerastuff : MonoBehaviour
{
	//The OG Technique Variable
	Transform thisTrans;
	//New Attempt

		/*
			public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
			public RotationAxes axes = RotationAxes.MouseXAndY;
			public float sensitivityX = 15F;
			public float sensitivityY = 15F;

			public float minimumX = -360F;
			public float maximumX = 360F;

			public float minimumY = -60F;
			public float maximumY = 60F;

			float rotationY = 0F;
		*/


	//Alternative technique Variables
	//[SerializeField] Transform player;
	//Transform cameraTrans;

	//'MadMax' Tutorial Mouse Rotation
	public float horizontalSpeed = 2.0f;
	public float verticalSpeed = 2.0f;

	void Start()
	{
		//OG Technique Initialization of Variable
		thisTrans = this.transform;
	}

	void Update()
	{
		//TIP: Can also do void LateUpdate for smoother camera work (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧

		//OG Technique Camera Rotate - except only one direction ¯\_(ツ)_/¯
		//thisTrans.rotation = Quaternion.Euler(0,40 * (Input.mousePosition.x/Screen.width), 0);
		//thisTrans.rotation = Quaternion.Euler(40 * (Input.mousePosition.y / Screen.height), 0, 0);


		//List of different techniques experimented with

		//New attempt
	/*		if (axes == RotationAxes.MouseXAndY)
			{
				float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

				rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
				rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

				transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
			}
			else if (axes == RotationAxes.MouseX)
			{
				transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
			}
			else
			{
				rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
				rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

				transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
	*/



			//Alternative Way - Transforms position in conjunction with player positioning (~˘▾˘)~
			//thisTrans.position = player.position;


			//Awful rotation (ʘᗩʘ')
			//thisTrans.Rotate(Vector3.right, 60 * Input.GetAxis("Mouse Y")/Screen.width);

			//thisTrans.Rotate(Vector3.up, 50 * Input.GetAxis("Mouse X")/Screen.height);

			//Tutorial Mouse Rotation ( ͡° ͜ʖ ͡°)
			float h = horizontalSpeed * Input.GetAxis("Mouse X");
			float v = verticalSpeed * Input.GetAxis("Mouse Y") ;
			transform.Rotate(v, h, 0);







		}
	}
//Add extra squigly bracket if 'New Attempt' is bracketted back into code
//}
