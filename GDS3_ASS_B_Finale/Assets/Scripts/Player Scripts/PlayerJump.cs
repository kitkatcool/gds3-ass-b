﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour {

    public float jumpForce ;
    private bool canJump = true;
    public Rigidbody rb;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {

    }

    void FixedUpdate()
    {
        if (Input.GetKey("space") && canJump == true)
        {
            rb.AddForce(0,jumpForce,0);
        }
    }
    void OnCollisionStay(Collision other)
    {
        canJump = true;
        //Debug.Log(canJump);
    }

    private void OnCollisionExit(Collision other)
    {
        canJump = false;
        //Debug.Log(canJump);
    }
}
