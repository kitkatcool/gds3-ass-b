﻿using UnityEngine;

public class YTCameraOrbit : MonoBehaviour {

	private Transform _XForm_Camera;
	private Transform _XForm_Parent;

//Stores location of camera pivot EVERY frame
	private Vector3 _LocalRotation;
	private float _CameraDistance = 10f;

	//Customisable Properties that control how fast camera pivots

		//how much camera orbits every frame
	public float MouseSensitivity = 4f;
		//the more you scroll the further it zooms
	public float ScrollSensitivity = 2f;
		//Control how long it takes for the camera to reach its destination, the bigger the number, the less time it takes to reach the destination
	public float OrbitDampening = 10f;
	public float ScrollDampening = 6f;
		//Whenever this is false, camera controls will work normally, when flipped to true, camera controls disable and allows different things to be done to the mouse
	public bool CameraDisabled = false;


	// Use this for initialization
	void Start () {
		//Sets transformation of Camera
		this._XForm_Camera = this.transform;
		//Sets transform of Camera PIVOT, ensure it's parented under an object otherwise it will throw a hissy fit
		this._XForm_Parent = this.transform.parent;
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//Disables camera on and off button
		if (Input.GetKeyDown(KeyCode.LeftShift))
			//Sets property to be opposite of whatever it currently is. If true, it will set it to false, if false, sets to true!
			CameraDisabled = !CameraDisabled;


		if (!CameraDisabled)
		{
			//Rotation of the Camera based on Mouse Co-ords
			//Make sure it doesn't equal 0 it triggers only when the mouse is NOT stationary
			if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
			{
				_LocalRotation.x += Input.GetAxis("Mouse X") * MouseSensitivity;
				_LocalRotation.y -= Input.GetAxis("Mouse Y") * MouseSensitivity;

				//Clamp the y Rotation to horizon and not flipping over at the top!
				//This does the same as the code below but is more efficient and easier to read ;)
				_LocalRotation.y = Mathf.Clamp(_LocalRotation.y, 0f, 90f);
			 
				//Messy Clamp example
				//If rotation is less than 0 rotation (below the horizon) we want to clamp it to be exactly 0!
				//if (_LocalRotation.y < 0f)
				//	_LocalRotation.y = 0f;
				//Else if clamps y rotation from going further than 90 degrees
				//else if (_LocalRotation.y > 90f)
				//	_LocalRotation.y = 90f;
			}
			//Zooming Input from our mouse Scroll Wheel
			if(Input.GetAxis("Mouse ScrollWheel") != 0f)
			{
				float ScrollAmount = Input.GetAxis("S") * ScrollSensitivity;
				//Dampens value of camera distance to object by 30% and multiplies scroll by that number! Allows fast scrolling further from object and smaller the closer you are :)
				ScrollAmount *= (this._CameraDistance * 0.3f);
				//negative 1 flips it in the opposite direction
				this._CameraDistance += ScrollAmount * -1f;
				//prevent the camera from going closer than 1.5 metres from target and won't go any further than 100 metres from the target
				//not having a minimum distance makes it act funny
				this._CameraDistance += Mathf.Clamp(this._CameraDistance, 1.5f, 100f);
			}
		}
		//Actual Camera Rig Orientation Transforms, MUST GO IN LAST UPDATE!
		//Sets pitch AND yaw of euler angles, Z is zero because we don't want rotation on Quaternion. Quaternion prevents Gimbal Lock! Woohoo!
		Quaternion QT = Quaternion.Euler(_LocalRotation.y, _LocalRotation.x, 0);
		this._XForm_Parent.rotation = Quaternion.Lerp(this._XForm_Parent.rotation, QT, Time.deltaTime * OrbitDampening);
		//Not necessary, but optimises the code by skipping the check every frame
		if (this._XForm_Camera.localPosition.z != this._CameraDistance * -1f) ;
		{
			//Moving between target values rather than setting it every frame
			this._XForm_Camera.localPosition = new Vector3(0f, 0f, Mathf.Lerp(this._XForm_Camera.localPosition.z, this._CameraDistance * -1f, Time.deltaTime * ScrollDampening));
		}
	}
}
