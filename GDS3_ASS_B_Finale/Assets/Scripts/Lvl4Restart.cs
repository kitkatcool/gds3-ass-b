﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lvl4Restart : MonoBehaviour {
    public GameObject level4Disable;
    public GameObject level1Enable;

    private void OnCollisionEnter(Collision player)
    {
        // Reset the player position.
        player.transform.position = Vector3.zero;

        // 2. Disable current level.
        level4Disable.SetActive(false);
        level1Enable.SetActive(true);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
