﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lvl4RotOP : MonoBehaviour {
	//This script rotates the 'chip' shaped objects below the 'Pipe' in the opposite direction
	public float rotateSpeed = 10f;

	void Start()
	{


	}

	void Update()
	{
		
		transform.RotateAround(transform.parent.position, new Vector3(0, -1, 0), rotateSpeed * Time.deltaTime);



	}
}
