﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lvl3Rotation : MonoBehaviour {
	//This script rotates the first Cylinder on Level3
	public float rotateSpeed = 10f;
	
	

	// Use this for initialization
	void Start () {

		
	}
	
	// Update is called once per frame
	void Update () {
		//Rotates weirdly, probably because vector3
		//transform.Rotate(Vector3.forward, rotateSpeed * Time.deltaTime, relativeTo);

		//Same result from before, doesn't rotate the right way
		//transform.Rotate(20 * Time.deltaTime, 0, 0);

		transform.RotateAround(transform.parent.position, new Vector3(0, 0, 1), rotateSpeed * Time.deltaTime);



	}
}
