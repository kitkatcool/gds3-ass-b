﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableLvl2 : MonoBehaviour {
	public GameObject level1Disable;
	public GameObject level2Enable;

	private void OnCollisionEnter(Collision player)
	{
		// Reset the player position.
		player.transform.position = Vector3.zero;

		// 1. Disable current level. . .
		level1Disable.SetActive(false);
        // 2. Enable next level!
        level2Enable.SetActive(true);
		//REMEMBER//NOTES//
		// you can add different particle effects that end the level and give them this same script that calls upon different game objects to enable and disable!
		// it's probably what you will have to do :) Note to self: make the gameobj variable PUBLIC! That means that ANYTHING can interact with it, therefore it
		// bridges a gap between the hierarchy game objects, and the scripts, allowing for you to let both influence each other! 
		// yeeeeees ok another note to self: ಥ_ಥ doughnut cri

	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
