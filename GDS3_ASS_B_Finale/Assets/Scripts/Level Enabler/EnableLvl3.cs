﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableLvl3 : MonoBehaviour {
	public GameObject level2Disable;
	public GameObject level3Enable;

	private void OnCollisionEnter(Collision player)
	{
		// Reset the player position.
		player.transform.position = Vector3.zero;

		// 2. Disable current level.
		level2Disable.SetActive(false);
		level3Enable.SetActive(true);
	}

		// Use this for initialization
		void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
