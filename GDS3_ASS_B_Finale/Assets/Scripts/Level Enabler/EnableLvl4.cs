﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableLvl4 : MonoBehaviour {
    public GameObject level3Disable;
    public GameObject level4Enable;

    private void OnCollisionEnter(Collision player)
    {
        // Reset the player position.
        player.transform.position = Vector3.zero;

        // 2. Disable current level.
        level3Disable.SetActive(false);
        level4Enable.SetActive(true);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
