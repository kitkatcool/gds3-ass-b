﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class Object_Rotation_Lerped : MonoBehaviour {
        //VECTOR 3 LERP DECLARED VARS
            public Transform startMarker;
            public Transform endMarker;
            //public float speed = 1.0F;
            public float startTime;
         public float journeyLength;
    //QUATERNION ROTATE LERP DECLARED VARS
        public Transform from;
        public Transform to;
        public float speed = 0.02F;
        

    

	// Use this for initialization
	void Start () {
        //VECTOR 3 LERP DEFINED(?) VARS
            startTime = Time.time;
            journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
		
	}

    // Update is called once per frame
    void Update() {
       //VECTOR 3 LERP 
            //float distCovered = (Time.time - startTime) * speed;
            //float fracJourney = distCovered / journeyLength;
            // transform.position = Vector3.Lerp(transform.position, endMarker.position, fracJourney);
       //VECTOR 3 ROTATION + Efficient Control Keys
            //this.transform.rotation += Vector3.right * Input.GetAxis("Horizontal");
       //Max's Rotation tutorial QUATERNION FOR LERP  
            this.transform.rotation *= Quaternion.Euler (Input.GetAxis("Vertical"), 0, 0);
            this.transform.rotation *= Quaternion.Euler (0, 0, -Input.GetAxis("Horizontal"));
       //QUATERNION LERP Application
            transform.rotation = Quaternion.Lerp(transform.rotation, to.rotation, speed);
        
            



        //Messy control practice
            /*if(Input.GetKey("a"))
        {
            this.transform.position += Vector3.right;
        }
            else if (Input.GetKey("d"))
        {
            this.transform.position += -Vector3.right;
        }
            */
    }
}
