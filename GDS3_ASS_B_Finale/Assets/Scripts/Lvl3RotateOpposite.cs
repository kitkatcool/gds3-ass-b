﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lvl3RotateOpposite : MonoBehaviour {
	//This script rotates the cylinder on Level3 opposite to the first rotation script
	public float rotateSpeed = 10f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		transform.RotateAround(transform.parent.position, new Vector3(0, 0, -1), rotateSpeed * Time.deltaTime);
	}
}
