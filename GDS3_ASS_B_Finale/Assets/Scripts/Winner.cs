﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Winner : MonoBehaviour {
	public GameObject level1Disable;
	public GameObject level2Enable;

	private void OnCollisionEnter(Collision player)
	{
		// Reset the player position.
		player.transform.position = Vector3.zero;

		//Disable current level and enable next
		level1Disable.SetActive(false);
		level2Enable.SetActive(true);
		//REMEMBER
		// you can add different particle effects that end the level and give them this same script that calls upon different game objects to enable and disable!
		// it's probably what you will have to do :) Note to self: make the gameobj variable PUBLIC! That means that ANYTHING can interact with it, therefore it
		// bridges a gap between the hierarchy game objects, and the scripts, allowing for you to let both influence each other! it's also waaay better than the
		// last method simply because changing things will be easier in the future, and you won't have to change random bits of code!!!!
		// YIPEE, NOW GO MY CHILD, BE FREE, AND REMEMBER: U are AWESOME and don't let the programming do your head in. Anyway, GOODNIGHT and remember to sleep well
		// yeeeeees ok another note to self: ಥ_ಥ doughnut cri
		

	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
